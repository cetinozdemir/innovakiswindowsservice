﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InnovaKis
{
    public class LogWriter
    {
        string path = @"C:\InnovaKisServiceApp\";
        public LogWriter()
        {
            if (!Directory.Exists(path + "\\log\\"))
            {
                Directory.CreateDirectory(path + @"\log\");
            }
        }

        public void write(string text)
        {
            try
            {
                using (StreamWriter w = File.AppendText(path + @"\log\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt"))
                {
                    w.WriteLine(text);
                    w.Flush();
                }
            }
            catch (Exception)
            {
                //	log("Log Sırasında hata oluştu:" + ex.Message);
            }
        }
    }
}
