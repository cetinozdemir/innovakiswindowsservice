﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net;
using System.ServiceProcess;

namespace InnovaKis
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        string version = "V0.1";

        string service_path = @"C:\InnovaKisServiceApp\";
        System.Timers.Timer tmrgenelislemler;
        List<CronJsonList> global_json = new List<CronJsonList>();

        protected override void OnStart(string[] args)
        {
            addLog("service", "service is started " + version);


            global_json = getJsonList();

            if (global_json.Count > 0)
            {
                tmrgenelislemler = new System.Timers.Timer();
                tmrgenelislemler.Interval = 60000;
                tmrgenelislemler.AutoReset = true;
                tmrgenelislemler.Enabled = true;
                tmrgenelislemler.Start();
                tmrgenelislemler.Elapsed += Tmrgenelislemler_Elapsed;

                addLog("json_read", "Json listesi okundu.");
            }
            else
            {
                addLog("json_read", "Hiç bir kayıt bulunmadığı için servis çalışmamaktadır. Json dosyasını güncelledikten sonra servisi restart etmelisiniz");
            }

        }


        protected override void OnStop()
        {
            addLog("service", "service is closed " + version);
        }

        private void Tmrgenelislemler_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var item in global_json)
            {
                if (item.daily)
                {
                    if (item.time == DateTime.Now.ToString("HH:mm"))
                    {
                        MakeHttpRequest(item.url);
                    }
                }
                else
                {
                    if (DateTime.Now.Minute % item.minute == 0)
                    {
                        MakeHttpRequest(item.url);
                    }
                }
            }
        }


        public void MakeHttpRequest(string url)
        {

            try
            {
                using (WebClient client = new WebClient())
                {
                    byte[] response = client.UploadValues(url, new NameValueCollection() { });
                   // string result = System.Text.Encoding.UTF8.GetString(response);
                }
            }
            catch (Exception ex)
            {
                addLog("webservis", "[175]: " + ex.Message);

            }
        }




        public List<CronJsonList> getJsonList()
        {
            List<CronJsonList> json = new List<CronJsonList>();
            try
            {
                string json_str = System.IO.File.ReadAllText(service_path + "service.json");

                if (json_str != "")
                {
                    json = JsonConvert.DeserializeObject<List<CronJsonList>>(json_str);
                }
            }
            catch (Exception ex)
            {
                addLog("read_json", "" + ex.Message);
            }
            return json;
        }

        public class CronJsonList
        {

            public bool daily { get; set; }
            public string time { get; set; }
            public int minute { get; set; }
            public string url { get; set; }
        }





        LogWriter lw = new LogWriter();
        public void addLog(string key, string value)
        {
            try
            {
                key = DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "\t" + key.PadRight(40 - key.Length) + "\t " + value;
                lw.write(key);
            }
            catch (Exception ex)
            {
                lw.write(DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "\t" + key.PadRight(40 - key.Length) + "\t " + value + " error: " + ex.Message);
            }
        }
    }
}
